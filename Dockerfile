# Base image: https://hub.docker.com/_/golang/
FROM golang:1.11.5-alpine3.9
MAINTAINER Geoff Johnson <geoff.jay@gmail.com>

COPY configs/configure.yaml /etc/plantd/
COPY plantd-configure-static /usr/bin/plantd-configure

CMD [ "/usr/bin/plantd-configure" ]
