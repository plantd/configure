PROJECT=configure
REGISTRY=registry.gitlab.com/plantd/plantd-$(PROJECT)
BUILD_TAG="$(shell git describe --all | sed -e's/.*\///g')"
PREFIX?=/usr
DESTDIR?=
CONFDIR=/etc
SYSTEMDDIR=/lib/systemd
VERSION := $(shell git describe)
BINARY_NAME := plantd-configure

M := $(shell printf "\033[34;1m▶\033[0m")

all: build

lint: ; $(info $M Linting the files)
	@./tools/checks lint

test: ; $(info $M Running unittests)
	@./tools/checks test

race: ; $(info $M Running data race detector)
	@./tools/checks race

msan: ; $(info $M Running memory sanitizer)
	@./tools/checks msan

coverage: ; $(info $M Generating global code coverage report)
	@./tools/coverage

coverhtml: ; $(info $M Generating global code coverage report in HTML)
	@./tools/coverage html

build: ; $(info $(M) Building project...)
	@env CGO_ENABLED=0 GOOS=linux GOARCH=amd64 \
	go build -a -o target/$(BINARY_NAME) -ldflags "-X main.VERSION=$(VERSION)" ./cmd/configure

static: ; $(info $(M) Building static executable...)
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build \
	-a -tags netgo -ldflags '-w -extldflags "-static"' \
	-o bin/plantd-configure-static cmd/configure/*.go

image: ; $(info $(M) Building application image...)
	@docker build -t $(REGISTRY) -f ./build/Dockerfile .

container: image ; $(info $(M) Running application container...)
	@docker run -p 4000:4000 $(REGISTRY):latest

install: ; $(info $(M) Installing plantd $(PROJECT) service...)
	@install -Dm 755 target/$(BINARY_NAME) "$(DESTDIR)$(PREFIX)/bin/plantd-$(PROJECT)"
	@install -Dm 644 README.md "$(DESTDIR)$(PREFIX)/share/doc/plantd/$(PROJECT)/README"
	@install -Dm 644 LICENSE "$(DESTDIR)$(PREFIX)/share/licenses/plantd/$(PROJECT)/COPYING"
	@install -Dm 644 configs/$(PROJECT).yaml "$(DESTDIR)$(CONFDIR)/plantd/$(PROJECT).yaml"
	@install -Dm 644 init/plantd-$(PROJECT).service "$(DESTDIR)$(SYSTEMDDIR)/system/plantd-$(PROJECT).service"

uninstall: ; $(info $(M) Uninstalling plantd $(PROJECT) service...)
	@rm $(DESTDIR)$(PREFIX)/bin/plantd-$(PROJECT)

clean: ; $(info $(M) Removing generated files...)
	@rm -rf bin/

.PHONY: all build container coverage coverhtml dep image lint test race msan install uninstall clean
