package context

import (
	"fmt"
	"log"
	"strings"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/viper"
)

type Config struct {
	AppName string

	ServerBind string
	ServerPort string

	DBHost string
	DBPort string
	DBName string

	TLSUse      bool
	TLSCert     string
	TLSCertAuth string
	TLSKey      string

	DebugMode bool
	LogFormat string
}

func LoadConfig(path string) (*Config, error) {
	home, err := homedir.Dir()
	if err != nil {
		return nil, err
	}

	config := viper.New()
	config.SetConfigName("configure")
	config.AddConfigPath(".")
	config.AddConfigPath("/etc/plantd")
	config.AddConfigPath(fmt.Sprintf("%s/.config/plantd", home))
	err = config.ReadInConfig()

	if err != nil {
		return nil, err
	}

	config.SetEnvPrefix("PLANTD_CONFIGURE")
	config.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	config.AutomaticEnv()

	var c Config

	c = Config{
		AppName: config.Get("app-name").(string),

		ServerBind: config.Get("server.bind").(string),
		ServerPort: config.Get("server.port").(string),

		DBHost: config.Get("db.host").(string),
		DBPort: config.Get("db.port").(string),
		DBName: config.Get("db.dbname").(string),

		TLSUse:      config.Get("tls.use").(bool),
		TLSCert:     config.Get("tls.cert").(string),
		TLSCertAuth: config.Get("tls.cert-auth").(string),
		TLSKey:      config.Get("tls.key").(string),

		DebugMode: config.Get("log.debug-mode").(bool),
		LogFormat: config.Get("log.log-format").(string),
	}

	log.Print("Configuration location: ", config.ConfigFileUsed())

	return &c, nil
}
