# Base image: https://hub.docker.com/_/golang/
# create: "docker build -t registry.gitlab.com/plantd/configure:fpm -f .gitlab-ci/fpm.Dockerfile ."
FROM golang:1.13.15-alpine3.12
# MAINTAINER Tom Depew <tom.depew@coanda.ca>

RUN apk update \
    && apk upgrade \
    && apk add \
        ruby \
        ruby-dev \
        ruby-etc \
        build-base \
        git \
        bash \
        tar \
    && gem install --no-document \
        fpm
