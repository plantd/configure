#!/bin/bash
# This script needs to be called from the repository root.

VERSION="$(git describe | sed -e 's/[vV]//g')" # version number cannot start with char.
BINARY_NAME="plantd-configure"
DESCRIPTION="Service that is part of plantd"
LICENSE=$"MIT License"
URL=$"https://gitlab.com/plantd/configure"
MAINTAINER=$"Mirko Moeller <mirko.moeller@coanda.ca>"
VENDOR=$""
INSTALLDIR="/tmp/stage" # Must tell make to put installation files here

if which go; then
  : # colon means do nothing
else
  echo "golang not installed or not reachable"
  exit 1
fi

if which fpm; then
  fpm --input-type dir --output-type deb \
    --name plantd-configure --version $VERSION \
    --package target \
    --description "$DESCRIPTION" \
    --license "$LICENSE" \
    --url "$URL" \
    --maintainer "$MAINTAINER" \
    --vendor "$VENDOR" \
    --after-install post/post-install.sh \
    --chdir "$INSTALLDIR"
else
  echo "fpm not installed or not reachable"
  exit 1
fi
